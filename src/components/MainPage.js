import { Link } from "react-router-dom";

function MainPage() {
    return (
        //This is a simple home page with just two buttons. The title and card are generated in App.js
        <div className="element-row">
            <Link to="/searchcity">
                <button className="basic-button">
                    Search by city
                </button>
            </Link>
            
            <Link to="/searchcountry">
                <button className="basic-button">
                    Search by country
                </button>
            </Link>
            
        </div>
    )
}

export default MainPage;