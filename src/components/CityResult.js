import React, { Fragment, useEffect } from "react";
import Geonames from 'geonames.js';
import { useHistory, useParams } from "react-router";
import BeatLoader from "react-spinners/BeatLoader";
import toast from "react-hot-toast";

function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

const CityResult = () => {
    const [city, setCity] = React.useState('')
    const [pop, setPop] = React.useState('')
    const { query } = useParams();
    const history = useHistory();

    useEffect(() => {
        const geonames = Geonames({
            username: 'weknowit',
            lan: 'en',
            encoding: 'JSON'
        });
        // promise
        geonames.search({q: query, featureClass: 'P'})
        .then(resp => {
            setCity(resp.geonames[0].name)
            setPop(resp.geonames[0].population)
        })
        .catch(err => {
            console.error(err)
            toast.error("Could not find city, please try again")
            history.push("/searchcity")
        });
    })

    return (
        <div className="element-col">
            {city === '' &&
                <BeatLoader size='15' />
            }
            {city !== '' &&
                <Fragment>
                    <p style={{'fontSize':'30px'}}>
                        {city}
                    </p>
                    <p style={{'fontSize':'12px'}}>
                        Population:
                    </p>

                    {numberWithSpaces(pop)}
                </Fragment>
                
            }
        </div>
    )
}

export default CityResult;