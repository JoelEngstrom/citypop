import { Link } from 'react-router-dom';
import './App.css';
import Routes from './Routes';
import 'react-notifications/lib/notifications.css';
import { Toaster } from 'react-hot-toast';

function App() {
    return (
        <div className="app">
            <Toaster/>
            <Link to="/" style={{ textDecoration: 'none' }}>
                <header>
                    CityPop
                </header>
            </Link>
            <div className="card">
                <Routes />
            </div>
        </div>
    );
}

export default App;