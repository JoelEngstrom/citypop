import React, { Fragment, useEffect } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import Geonames from 'geonames.js';
import BeatLoader from "react-spinners/BeatLoader";
import toast from "react-hot-toast";

const CountryResult = props => {
    const [country, setCountry] = React.useState('')
    const [city1, setCity1] = React.useState('')
    const [city2, setCity2] = React.useState('')
    const [city3, setCity3] = React.useState('')
    const cities = [city1, city2, city3]
    const { query } = useParams();
    const history = useHistory();

    useEffect(() => {
        const geonames = Geonames({
            username: 'weknowit',
            lan: 'en',
            encoding: 'JSON'
        });

        //This resolves once both a country and cities have been found
        geonames.search({q: query, featureCode: 'PCLI'})
        .then(resp => {
            var c = resp.geonames[0]
            setCountry(c.name)
            return geonames.search({q: '', country: c.countryCode, orderby: 'population', featureClass: 'P'})
        })
        .then(resp => {
            setCity1(resp.geonames[0].name)
            setCity2(resp.geonames[1].name)
            setCity3(resp.geonames[2].name)
        })
        .catch(err => {
            console.error(err)
            toast.error("Could not find country, please try again")
            history.push("/searchcountry")
        });
    }, [query, history])
    
    return (
        <div className="element-col">
            {city3 === '' &&
                <BeatLoader size='15' />
            }
            {city3 !== '' &&
                <Fragment>
                    <p style={{'fontSize':'30px'}}>
                        {country}
                    </p>

                    {cities.map((city, index) => {
                        return (
                            <Link key={index} to={"/cityresult/" + city}>
                                <button key={index} className="country-button">
                                    {city}
                                </button>
                            </Link>
                        )
                    })}
                </Fragment>
            }
        </div>
    )
}

export default CountryResult;