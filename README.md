[Website](https://citypop.joelengstrom.se/)

This is a website for finding the populations of various cities. It was developed using ReactJS, for the purposes of a job application.

It requires npm to run. Clone the repository, then use your terminal to type:
```
cd /path/to/directory
npm install
npm start
```
